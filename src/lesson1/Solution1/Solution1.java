package lesson1.Solution1;

import java.util.Scanner;

public class Solution1
{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите целочисленное делимое: ");
        int n = in.nextInt();
        System.out.println("Введите целочисленный делитель: ");
        int j = in.nextInt();
        System.out.println(n+" / "+j+" = "+n/j+" и "+n%j+" в остатке");
    }
}

