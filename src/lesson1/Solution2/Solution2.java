package lesson1.Solution2;

import java.util.Scanner;

public class Solution2 {
    public static void main(String[] args) {
        System.out.println("Введите число с ненулевой дробной частью: ");
        Scanner in = new Scanner(System.in);
        double n = in.nextDouble();
        if ((n-(int)n) >= 0.5){
            System.out.print("Ближайшее целое число: ");
            System.out.println((int)n+1);
        }
        else{
            System.out.print("Ближайшее целое число: ");
            System.out.println((int)n);
        }
    }
}

