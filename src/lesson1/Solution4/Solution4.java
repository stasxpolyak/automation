package lesson1.Solution4;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        System.out.println("Введите двузначное число: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int sum = 0;
        if (n > 9){
            if (n < 100){
                while (n > 0){
                    sum = sum + n%10;
                    n = (n-sum)/10;
                }
                System.out.println("Сумма цифр равна "+sum);
            }
            else{
                System.out.println("Число с количеством цифр более 2х.");
            }
        }
        else{
            System.out.println("Число с 1 цифрой.");
        }
    }
}

