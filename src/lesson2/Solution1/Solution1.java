package lesson2.Solution1;

public class Solution1 {
    public static void main(String[] args) {
        String text = "Рассерженный Родион Романович раскричался: «Рано радовались, размокший ремень рюкзака разорвался».";
        //замена р в нижнем регистре
        //String out1 = text.replace('р', '*');
        //замена Р в верхнем регистре
        //String out2 = text.replace('Р', '*');
        System.out.println(text.replace('Р','*').replace('р','*'));
    }
}

