package lesson2.Solution2;

public class Solution2 {
    public static void main(String[] args) {
        String text = "Рассерженный Родион Романович раскричался: «Рано радовались, размокший ремень рюкзака разорвался».";
        char[] chars = text.toCharArray();
        for(int i = 0; i < chars.length; i++){
            if (chars[i] == 'р'||chars[i] == 'Р') chars[i] = '*';
        }
        System.out.println(new String(chars));
    }
}

