package lesson2.Solution3;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        int r;
        Scanner in = new Scanner(System.in);
        boolean check = true;
        while (check) {
            //рандомное число берется из [0;1), нам нужно получить из [5,155] -> (int)[0,1)*151 + 5 <=> [0, 150] + 5
            r = (int) (Math.random() * 151) + 5;
            if (r > 25 && r < 100) System.out.println("Число " + r + " содержится в интервале (25;100)");
            else System.out.println("Число " + r + " не содержится в интервале (25;100)");
            System.out.println(r);
            System.out.println("Для выхода нажмите кнопку 'q' и затем 'Enter', либо для продолжения любую клавишу и затем 'Enter'");
            if ("q".equals(new String(in.next()))) break;
            else continue;
        }
    }
}

