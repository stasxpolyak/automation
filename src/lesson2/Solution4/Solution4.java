package lesson2.Solution4;

public class Solution4 {
    //sort by insertion
    private static void insertSort(char[] arr){
        char temp;
        int j;
        for(int i = 0; i < arr.length-1; i++){
            if (arr[i] > arr[i + 1]) {
                temp = arr[i + 1];
                arr[i + 1] = arr[i];
                j = i;
                while (j > 0 && temp < arr[j - 1]) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = temp;
            }
        }
    }
    //format output an array (Ex., "['a','b','c']")
    private static void output(char[] arr){
        for(int i = 0; i < arr.length; i++){
            if (i == 0) System.out.print("[\'"+arr[i]+"\',");
            if (i > 0&&i<arr.length-1) System.out.print("\'"+arr[i]+"\',");
            if (i == arr.length-1) System.out.print("\'"+arr[i]+"\']");
        }
    }

    public static void main(String[] args) {
        String text = "Я у мамы программист";
        String tmp = "";
        for(int i=0; i < text.toLowerCase().length(); i++){
            if (tmp.indexOf(text.toLowerCase().charAt(i)) == -1&&Character.isLetter(text.toLowerCase().charAt(i))){
                tmp = tmp + text.toLowerCase().charAt(i);
            }
        }
        char[] uniqArray = tmp.toCharArray();
        insertSort(uniqArray);
        output(uniqArray);
    }
}

