package lesson3;

import java.util.StringJoiner;

public class Cat extends Pet{
    //fields
    protected String owner;
    protected String loc;
    enum SEX
    {
        Male("Кот"),
        Female("Кошка"),
        Kitten("Котенок");
        private String value;
        SEX(String value){
            this.value = value;
        }
        public String sex(){
            return value;
        }
    }
    protected SEX sex;

    //constructors
    Cat(){}
    Cat(String name, int age, SEX sex, String owner, String loc){
        super(name, age);
        this.sex = sex;
        this.owner = owner;
        this.loc = loc;
        //this.health = 100;
    }
    Cat(Cat ob){
        super(ob.name, ob.age);
        this.sex = ob.sex;
        this.owner = ob.owner;
        this.loc = ob.loc;
        this.strength = ob.strength;
    }
    //methods
    //кот ест
    @Override
    public void eat(){
        System.out.println("I\'am "+ this.name+ ". I'am eating.");
    }
    //кот бежит
    @Override
    public void run(){
        System.out.println("I\'am "+this.name+". I\'am running");
    }
    //кот мяукает
    @Override
    public void voice(){
        System.out.println(this.name + ": meow");
    }
    //кот мяукает несколько раз
    public void voice(int n) throws VoiceException
    {
        StringJoiner tmp = new StringJoiner("-");
        if (n>0) {
            System.out.print(this.name+": ");
            while (n > 0) {
                tmp.add("meow");
                n--;
            }
        }
        else throw new VoiceException(n);
        System.out.println(tmp.toString());
    }
    //кот говорит и мяукает
    public void voice(String say)
    {
        System.out.println(this.name+": "+say+"... meow.");
    }
    //информация о представителе
    public void info(){
        System.out.println("Имя:              "+this.name);
        System.out.println("Возраст:          "+this.age);
        System.out.println("Пол:              "+this.sex.sex());
        System.out.println("Имя хозяина:      "+this.owner);
        System.out.println("Место жительства: "+this.loc);
        System.out.println("Здоровье:         "+this.health);
        System.out.println("Сила удара:       "+this.strength);
    }
    @Override
    public void fight(Cat cat) throws ArgException
    {
        if (this != cat) {
            System.out.println(this.name + "(" + this.health + ")" + " vs " + "(" + this.health + ")" + cat.name);
            int i = 1;
            while ((this.health > cat.strength) && (cat.health > this.strength)) {
                if (i % 2 == 0) {
                    System.out.println("Шаг " + i + ": " + this.name + " наносит удар с силой " + this.strength + ".");
                    cat.health = cat.health - this.strength;
                    System.out.println(this.name + "(" + this.health + ")" + " vs " + "(" + cat.health + ")" + cat.name);
                } else {
                    System.out.println("Шаг " + i + ": " + cat.name + " наносит удар с силой " + cat.strength + ".");
                    this.health = this.health - cat.strength;
                    System.out.println(this.name + "(" + this.health + ")" + " vs " + "(" + cat.health + ")" + cat.name);
                }
                i++;
            }
            if (this.health <= cat.strength && cat.health > this.strength) {
                System.out.println("Победитель схватки: "+cat.name);
            } else {
                System.out.println("Победитель схватки: "+this.name);
            }
        }
        else {
            throw new ArgException();
        }
    }

    @Override
    public void fight(Dog dog) {
        System.out.println(this.name + "("+ this.health +")" + " vs " + "("+dog.health+")" + dog.name);
        System.out.println(this.name + " слинял.");
        System.out.println("Победитель схватки: "+dog.name);
    }
}
