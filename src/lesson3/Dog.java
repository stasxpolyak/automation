package lesson3;

import java.util.StringJoiner;

public class Dog extends Pet {

    enum HOMESTATUS {
        Home("Домашняя собака"),
        Homeless("Бездомная собака");
        private String status;
        HOMESTATUS(String status){
            this.status = status;
        }
        public String status(){
            return status;
        }
    }
    public HOMESTATUS homestatus;

    //constructors
    Dog(){}
    Dog(String name, int age, HOMESTATUS homestatus){
        super(name, age);
        this.homestatus = homestatus;
    }
    Dog(int age, HOMESTATUS homestatus){
        this.name = "\"Без имени\"";
        this.age = age;
        this.homestatus = homestatus;
        this.strength = (int)(Math.random()*10)+1;
    }
    //methods
    @Override
    public void eat()
    {
        System.out.println(this.name+": I am dog. I am eating.");
    }
    @Override
    public void run(){
        System.out.println(this.name+": I am dog. I am running.");
    }
    @Override
    public void voice(){
        System.out.println(this.name+": hawk");
    }
    public void voice(int n){
        System.out.print(this.name+": ");
        StringJoiner tmp = new StringJoiner("-");
        while(n>0){
            tmp.add("hawk");
            n--;
        }
        System.out.println(tmp.toString());
    }
    //информация о представителе
    public void info(){
        System.out.println("Имя:        "+this.name);
        System.out.println("Возраст:    "+this.age);
        System.out.println("Статус:     "+this.homestatus.status());
        System.out.println("Здоровье:   "+this.health);
        System.out.println("Сила удара: "+this.strength);
    }
    @Override
    public void fight(Cat cat) {
        System.out.println(this.name + "("+ this.health +")" + " vs " + "("+cat.health+")" + cat.name);
        System.out.println(cat.name + " слинял.");
        System.out.println("Победитель схватки: "+ this.name);
    }
    @Override
    public void fight(Dog dog) throws ArgException
    {
        if (this != dog) {
            System.out.println(this.name + "(" + this.health + ")" + " vs " + "(" + this.health + ")" + dog.name);
            int i = 1;
            while ((this.health > dog.strength) && (dog.health > this.strength)) {
                if (i % 2 == 0) {
                    System.out.println("Шаг " + i + ": " + this.name + " наносит удар с силой " + this.strength + ".");
                    dog.health = dog.health - this.strength;
                    System.out.println(this.name + "(" + this.health + ")" + " vs " + "(" + dog.health + ")" + dog.name);
                } else {
                    System.out.println("Шаг " + i + ": " + dog.name + " наносит удар с силой " + dog.strength + ".");
                    this.health = this.health - dog.strength;
                    System.out.println(this.name + "(" + this.health + ")" + " vs " + "(" + dog.health + ")" + dog.name);
                }
                i++;
            }
            if (this.health <= dog.strength && dog.health > this.strength) {
                System.out.println("Победитель схватки: "+dog.name);
            } else {
                System.out.println("Победитель схватки: "+this.name);
            }
        }
        else{
            throw new ArgException();
        }
    }
}
