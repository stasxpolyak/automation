package lesson3;

public interface Fightable{
    abstract void fight(Cat cat) throws ArgException;
    abstract void fight(Dog dog) throws ArgException;
}
