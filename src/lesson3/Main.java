package lesson3;

public class Main {

    public static void main(String[] args) throws ArgException, VoiceException {
        //Создаем кота
        Cat cat1 = new Cat("Барсик", 2, Cat.SEX.Male, "Пятачок", "Нора кролика");

        //создаем кошку
        Cat cat2 = new Cat("Матроскин", 3, Cat.SEX.Female, "Дядя Федор", "Простоквашино");

        //создаем бездомную собаку с именем
        Dog dog1 = new Dog("Бобик", 9, Dog.HOMESTATUS.Homeless);

        //создаем домашнюю собаку без имени
        Dog dog2 = new Dog(4, Dog.HOMESTATUS.Home);

        //далее выводим информацию о каждом питомце
        System.out.println("============================");
        cat1.info();
        System.out.println("============================");
        cat2.info();
        System.out.println("============================");
        dog1.info();
        System.out.println("============================");
        dog2.info();
        System.out.println("============================");

        //методы классов Cat и Dog
        cat1.run();
        cat2.eat();
        cat1.voice();
        cat1.voice(5);
        cat2.voice("I am hungry!");
        dog1.run();
        dog2.eat();
        dog1.voice();
        dog2.voice(3);
        System.out.println("============================");

        //битва домашней собаки против бездомной
        dog1.fight(dog2);
        System.out.println("============================");

        //битва Барсика против Матроскина
        cat1.fight(cat2);
        System.out.println("============================");

        //битва собаки против кота
        dog1.fight(cat1);
        System.out.println("============================");

        //ловля исключения ArgException в методе fight интерфейса Fightable
        try {
            dog2.fight(dog2);
        }
        catch (ArgException e){
            System.out.println("Перехвачено исключение: "+e);
        }

        //ловля исключения VoiceException в методе voice класса Cat
        try{
            cat1.voice(-6);
        }
        catch (VoiceException e){
            System.out.println("Перехвачено исключение: "+e);
        }
    }
}
