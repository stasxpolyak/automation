package lesson3;

public abstract class Pet implements Fightable{
    protected String name;
    protected int age;
    protected int health = 100;
    protected int strength;
    //constructor
    Pet(){}
    Pet(String name, int age){
        this.name = name;
        this.age = age;
        this.strength = (int)(Math.random()*10)+1;
    }
    Pet(Pet ob){
        this.name = ob.name;
        this.age = ob.age;
        this.health = ob.health;
        this.strength = ob.strength;
    }

    //methods
    public void eat(){
        System.out.println("Pet is eating.");
    }
    public void run(){
        System.out.println("Pet is running.");
    }
    public void voice()
    {
        System.out.println("I am not talk.");
    }
}
