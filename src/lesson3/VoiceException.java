package lesson3;

public class VoiceException extends Exception{
    private int detail;
    VoiceException(int n){
        detail = n;
    }
    @Override
    public String toString(){
        return "VoiceException: incorrect parameter n=" + detail + " for method voice  ";
    }
}
