package lesson5;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
            Shelter shelter = new Shelter();
            Cat cat1 = new Cat("Kesha",Pet.HOMESTATUS.Home, "black");
            Cat cat2 = new Cat("Matroskin", Pet.HOMESTATUS.Homeless, "grey");
            Cat cat3 = new Cat("Barsik", Pet.HOMESTATUS.Homeless, "orange");
            Dog dog1 = new Dog("Bobik",Pet.HOMESTATUS.Homeless,"white");
            Dog dog2 = new Dog("Rex", Pet.HOMESTATUS.Homeless, "black");
            Dog dog3 = new Dog("Hatiko", Pet.HOMESTATUS.Homeless, "piebald");
            shelter.addPet(cat1);
            shelter.addPet(cat2);
            shelter.addPet(cat3);
            shelter.addPet(dog1);
            shelter.addPet(dog2);
            shelter.addPet(dog3);
            System.out.println("Added homeless pets to shelter.");
            System.out.println("Quantity pets of shelter: "+shelter.size());
            System.out.println("=========================");
            System.out.println("Take random pet from shelter");
            shelter.getPetRandom();
            System.out.println("Quantity pets of shelter: "+shelter.size());
            System.out.println("=========================");
            System.out.println("Take defined pet from shelter.");
            shelter.getPet("Cat","grey");
            System.out.println("Quantity pets of shelter: "+shelter.size());
            System.out.println("=========================");
            System.out.println("List pets of shelter.");
            shelter.listPet();
        }

    }

