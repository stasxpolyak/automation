package lesson5;

public abstract class Pet {
    public HOMESTATUS homestatus;
    String name;
    String color;
    enum HOMESTATUS{
        Home("Домашний питомец"),
        Homeless("Бездомный питомец");
        protected String status;
        HOMESTATUS(String status){
            this.status = status;
        }
        public String getStatus(){
            return status;
        }
    }
    Pet(){}
    Pet(String name, HOMESTATUS homestatus, String color){
        this.name = name;
        this.color = color;
        this.homestatus = homestatus;
    }
    String getType(){
        return "Pet";
    }
}
