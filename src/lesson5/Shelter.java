package lesson5;

import java.util.HashMap;
import java.util.Map;

public class Shelter {
    private HashMap<Integer, Pet> shelter= new HashMap<Integer, Pet>();
    public static int counter = 1;

    public void addPet(Pet pet){
        if (pet.homestatus == Pet.HOMESTATUS.Homeless) {
            shelter.put(counter, pet);
            counter++;
        }
    }

    public void addPet(int key, Pet pet){
        if (pet.homestatus == Pet.HOMESTATUS.Homeless) {
            shelter.put(key, pet);
        }
    }

    public void removePet(Pet pet){
        int key=0;
        for(Map.Entry<Integer, Pet> item: shelter.entrySet()){
            Object ob = item.getValue();
            if (ob.equals(pet)){
                key = item.getKey();
            }
        }
        shelter.remove(key);
    }

    public void listPet(){
        for(Map.Entry<Integer, Pet> item: shelter.entrySet()){
            Object ob = item.getValue();
            if (ob instanceof Cat) {
                Cat cat = (Cat)ob;
                System.out.println("["+item.getKey()+"]: Name:  "+cat.getType()+" "+cat.name + "\n     Color: " + cat.color);
            }
            if (ob instanceof Dog){
                Dog dog = (Dog)ob;
                System.out.println("["+item.getKey()+"]: Name:  "+dog.getType()+" "+dog.name + "\n     Color: " + dog.color);
            }
        }
    }

    public void getPetRandom(){
        int key = (int)(Math.random()*(shelter.size()))+1;
        Pet pet = shelter.get(key);
        if (pet instanceof Cat) {
            Cat cat = (Cat)pet;
            System.out.println("["+key+"]: Name:  "+cat.getType()+" "+cat.name + "\n     Color: " + cat.color);
        }
        if (pet instanceof Dog){
            Dog dog = (Dog)pet;
            System.out.println("["+key+"]: Name:  "+dog.getType()+" "+dog.name + "\n     Color: " + dog.color);
        }
        shelter.remove(key);
    }

    public void getPet(String type, String color){
        int key=0;
        for(Map.Entry<Integer, Pet> item: shelter.entrySet()){
            Pet pet = item.getValue();
            if ((pet.getType()==type)&&(pet.color==color)){
                key = item.getKey();
                System.out.println("["+key+"]: Name:  "+pet.getType()+" "+pet.name + "\n     Color: " + pet.color);
                continue;
            }
        }
        if (key == 0){
            System.out.println("Pet is not found.");
        }
        shelter.remove(key);
    }

    public int size(){
        int sz=0;
        for(Map.Entry<Integer, Pet> item: shelter.entrySet()){
            sz++;
        }
        return sz;
    }
}
